package com.example.mycounter;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    /** TODO: declare integer mCount that can be incremented and reset
    *   TODO: declare TextView mTextView that will be used to change the displayed count
    */


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //find the View that has ID of the TextView that shows the count.
    }

    //TODO: Add click handler method that adds one to the count
    //TODO: Add click handler method that resets the count

}